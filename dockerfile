FROM openjdk:11.0.13

WORKDIR /app

COPY . .

CMD ["java", "Main.java"]